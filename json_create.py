import os
import json

def extract_information(readme_path):
    if os.path.exists(readme_path):
        with open(readme_path, "r") as readme_file:
            return readme_file.read()
    else:
        return "No README found."
def get_sub_files(folder_path):
    l = []
    for item in os.listdir(folder_path):
        if "latest" not in item:
            l.append(item)
    return l
def find_binary_files(folder_path):
    binary_files = []
    
    for root, _, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if os.path.isfile(file_path) and file_name.endswith(".bin"):
                binary_files.append(file_name)
    
    return binary_files

def create_nested_structure(folder_path):
    if not os.path.isdir(folder_path):
        return None
    
    folder_name = os.path.basename(folder_path)
    subitems = {}
    for item in os.listdir(folder_path):
        item_path = os.path.join(folder_path, item)
        binary_files = find_binary_files(folder_path)
        if os.path.isdir(item_path):
            subitems[item]  = create_nested_structure(item_path)
        else:
            if "Readme" in item:
                item = item + "=" + extract_information(item_path)
            subitems[item] = None
    if len(subitems) > 0 and subitems[item] == None:
        l = list(subitems.keys())
        if len(l) > 1:
            subitems = {"BIN_FILE" : [],"Release_note" : "","VIN_START" : -1,"VIN_END" : -1}
            for i in l:
                if "Readme" in i:
                    if len(i.split("=")) > 1:
                        subitems["Release_note"] = i.split("=")[1]
                elif "Configuration" in i:
                    item_path_new = os.path.join(folder_path, i)
                    d = extract_information(item_path_new)
                    subitems["VIN_START"] = int(d.split(",")[0].split("=")[1])
                    subitems["VIN_END"] = int(d.split(",")[1].split("=")[1])
                else:
                    subitems["BIN_FILE"].append(i)
        else:
            # print(subitems)
            if "Component_info" in item:
                item_path_new = os.path.join(folder_path, item)
                # print(extract_information(item_path_new))
                d = extract_information(item_path_new)
                sub_files = get_sub_files(os.path.dirname(folder_path))
                # if "," in d:
                #     pass
                # else:
                return {"latest_version_file" : d.split(",")[0].split("=")[1],"latest_version_no" : int(d.split(",")[1].split("=")[1]),"BIN_FILES" : sub_files}
                # print(subitems,d)
                # return extract_information(item_path_new).split("=")[1]
            else:
                subitems = {"BIN_FILE" : list(subitems.keys())}
        # print(subitems)
    return subitems
Component_list = ["BMS","VCU","ICL","Charger","MCU","MVCU","TEL"]
def save_json(data, output_file):
    with open(output_file, "w") as json_file:
        json.dump(data, json_file, indent=2)

if __name__ == "__main__":
    source_folder_path = "Software Update"  # Path to your root folder
    output_json_file = "output.json"

    nested_structure = create_nested_structure(source_folder_path)

    
    save_json(nested_structure, output_json_file)

    print("JSON file with nested object structure created successfully.")


